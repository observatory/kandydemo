package com.observatory.kandydemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.genband.kandy.api.Kandy;
import com.genband.kandy.api.services.calls.IKandyCall;
import com.genband.kandy.api.services.calls.IKandyIncomingCall;
import com.genband.kandy.api.services.calls.KandyCallServiceNotificationListener;
import com.genband.kandy.api.services.calls.KandyCallState;
import com.observatory.utils.BaseActivity;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class CallScreen extends BaseActivity implements KandyCallServiceNotificationListener {


    private Button btnCall;
    private EditText edtEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_screen);
        edtEmail.setText("@observatory.com");

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCallActivity();

            }
        });

        Kandy.getServices().getCallService().registerNotificationListener(this);



    }


    @Override
    protected void onLayout() {
        super.onLayout();

        btnCall = (Button) findViewById(R.id.btnCall);
        edtEmail = (EditText) findViewById(R.id.edtEmail);

    }


    private void startCallActivity() {

       String strEmail=edtEmail.getText().toString().trim();

        if (strEmail.equals("")||strEmail.equals("@observatory.com")){
            Crouton.makeText(CallScreen.this,"Please enter email", Style.ALERT);
            return;
        }
        edtEmail.setText("@observatory.com");

        Intent intent = new Intent(context, Dialer.class);
        intent.putExtra(Dialer.EMAIL,strEmail);

        startActivity(intent);

    }


    @Override
    public void onIncomingCall(IKandyIncomingCall iKandyIncomingCall) {

    }

    @Override
    public void onCallStateChanged(KandyCallState kandyCallState, IKandyCall call) {

    }

    @Override
    public void onVideoStateChanged(IKandyCall call, boolean b, boolean b2) {

    }

    @Override
    public void onAudioStateChanged(IKandyCall call, boolean b) {

    }
}
