package com.observatory.kandydemo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.genband.kandy.api.Kandy;
import com.genband.kandy.api.services.calls.IKandyCall;
import com.genband.kandy.api.services.calls.IKandyIncomingCall;
import com.genband.kandy.api.services.calls.IKandyOutgoingCall;
import com.genband.kandy.api.services.calls.KandyCallResponseListener;
import com.genband.kandy.api.services.calls.KandyCallServiceNotificationListener;
import com.genband.kandy.api.services.calls.KandyCallState;
import com.genband.kandy.api.services.calls.KandyRecord;
import com.genband.kandy.api.services.calls.KandyView;
import com.observatory.utils.BaseActivity;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class Dialer extends BaseActivity implements KandyCallServiceNotificationListener {




    public static String EMAIL="email";

    private Button btnEnd;
    private KandyView kandyMyView,kandyFriendView;

    private Button btnCall;
    private EditText edtEmail;


    private boolean isVideo=true;

    private IKandyIncomingCall currentInCall;
    private IKandyOutgoingCall outCall;

    private String tag="Dialer";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createDialScreen();
        Kandy.getServices().getCallService().registerNotificationListener(this);


    }





    public void createDialScreen(){

        setContentView(R.layout.activity_call_screen);
        btnCall = (Button) findViewById(R.id.btnCall);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtEmail.setText("@observatory.com");

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                placeCall();
            }
        });

    }



    public void createCallScreen(){

        setContentView(R.layout.activity_dialer);

        btnEnd=(Button)findViewById(R.id.btnEnd);
        kandyMyView=(KandyView)findViewById(R.id.kandyMyView);
        kandyFriendView=(KandyView)findViewById(R.id.kandyFriendView);


        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                outCall.hangup(new KandyCallResponseListener() {
                    @Override
                    public void onRequestSucceeded(IKandyCall call) {
                    }

                    @Override
                    public void onRequestFailed(IKandyCall call, int responseCode, String err) {




                    }
                });
            }
        });



    }


    private void placeCall(){

        String strEmail=edtEmail.getText().toString().trim();

        if (strEmail.equals("")||strEmail.equals("@observatory.com")){
            Crouton.showText(Dialer.this,"Please enter email", Style.ALERT);
            return;
        }


        edtEmail.setText("@observatory.com");

        createCallScreen();

        Log.v(tag,"placeCall:"+strEmail);

       KandyRecord record=new KandyRecord(strEmail);
       outCall= Kandy.getServices().getCallService().createVoipCall(record,isVideo);
       bindView(outCall);

        outCall.establish(new KandyCallResponseListener() {
            @Override
            public void onRequestSucceeded(IKandyCall iKandyCall) {
                Log.v(tag,"onRequestSucceeded");



            }

            @Override
            public void onRequestFailed(IKandyCall iKandyCall, int responseCode, String err) {
                Log.v(tag,"Call Failed:"+err);

            }
        });




    }


/*    private void callConnected(){

       *//* btnEnd.setVisibility(View.VISIBLE);
      *//**//*  btnCall.setVisibility(View.GONE);
        edtEmail.setVisibility(View.GONE);*//*


    }*/
    /*private void callDisconnected(){
        createDialScreen();



    }
*/

    private void bindView(IKandyCall call){
        call.setLocalVideoView(kandyMyView);
        call.setRemoteVideoView(kandyFriendView);

    }


    @Override
    public void onIncomingCall(IKandyIncomingCall call) {

        currentInCall=call;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createCallScreen();

                bindView(currentInCall);

                currentInCall.accept(true,new KandyCallResponseListener() {
                    @Override
                    public void onRequestSucceeded(IKandyCall call) {

                    }

                    @Override
                    public void onRequestFailed(IKandyCall call, int responseCode, String err) {

                    }
                });

            }
        });




    }

    @Override
    public void onCallStateChanged(KandyCallState kandyCallState, IKandyCall call) {


        Log.v(tag,"onCallStateChanged:"+KandyCallState.TERMINATED+" : "+kandyCallState );


        if (kandyCallState==KandyCallState.TERMINATED){

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    createDialScreen();
                }
            });

        }


    }

    @Override
    public void onVideoStateChanged(IKandyCall call, boolean isReceivingVideo, boolean isSendingVideo) {

    }

    @Override
    public void onAudioStateChanged(IKandyCall call, boolean onMute) {

    }
}
