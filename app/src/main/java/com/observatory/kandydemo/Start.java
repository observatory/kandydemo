package com.observatory.kandydemo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.genband.kandy.api.Kandy;
import com.genband.kandy.api.access.KandyLoginResponseListener;
import com.genband.kandy.api.services.calls.KandyRecord;
import com.observatory.utils.BaseActivity;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


public class Start extends BaseActivity {

    private EditText edtEmail, edtPassword;
    private Button btnLogin;

    public static final String IS_LOGGED_IN = "logged_in";

    private ProgressDialog progressDialog;

    private String tag="Start";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        actionBar.hide();

       /* if (getSavedBoolean(IS_LOGGED_IN)) {

            Intent intent = new Intent(context, Dialer.class);
            startActivity(intent);

        }*/

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });


    }

    @Override
    protected void onLayout() {
        super.onLayout();
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        edtEmail.setText("rohit@observatory.com");
        edtPassword.setText("rohit@123");


        btnLogin = (Button) findViewById(R.id.btnLogin);

    }

    private void login() {


        String strEmail = edtEmail.getText().toString().trim();
        String strPassword = edtPassword.getText().toString().trim();


        if (strEmail.equals("")) {

            Crouton.showText(this, "Please enter emailid", Style.ALERT);
            return;
        }

        if (strPassword.equals("")) {

            Crouton.showText(this, "Please enter password", Style.ALERT);
            return;
        }


        progressDialog = ProgressDialog.show(context, "DEMO", "Please wait...");

        Kandy.getAccess().login(new KandyRecord(strEmail), strPassword, new KandyLoginResponseListener() {
            @Override
            public void onLoginSucceeded() {
                progressDialog.dismiss();
                saveBoolean(IS_LOGGED_IN, true);
                finish();
                Intent intent = new Intent(context, Dialer.class);
                startActivity(intent);


            }

            @Override
            public void onRequestFailed(int responseCode, String err) {
                progressDialog.dismiss();
                Log.v(tag,"err:"+err);
            //    Crouton.showText(Start.this, "Login failed because of " + err, Style.ALERT);


            }
        });


    }
}
