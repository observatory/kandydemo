package com.observatory.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.genband.kandy.api.Kandy;
import com.genband.kandy.api.access.KandyLogoutResponseListener;
import com.observatory.kandydemo.R;

/**
 * Created by rohit on 12/12/14.
 */
public class BaseActivity extends ActionBarActivity {

    private static String PREFS="prefs";


    protected ActionBar actionBar;
    protected Context context;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.trans_blue)));
        int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        TextView abTitle = (TextView) findViewById(titleId);
        abTitle.setTextColor(getResources().getColor(R.color.white));


    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        onLayout();
    }


    protected void onLayout(){

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



    protected void logoutKandy(){

        Kandy.getAccess().logout(new KandyLogoutResponseListener() {

            @Override
            public void onRequestFailed(int responseCode, String err) {
                //TODO insert your code here
            }

            @Override
            public void onLogoutSucceeded() {
                //TODO insert your code here
            }
        });

    }


    public void saveBoolean(String name,boolean value){

        SharedPreferences sharedpreferences = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(name,value);
        editor.commit();

    }

    public boolean getSavedBoolean(String name){
        SharedPreferences sharedpreferences = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(name,false);
    }

    public void saveString(String name,String value){

        SharedPreferences sharedpreferences = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(name,value);
        editor.commit();

    }

    public String getSavedString(String name){
        SharedPreferences sharedpreferences = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return sharedpreferences.getString(name,"");
    }




}
